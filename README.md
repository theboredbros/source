# README #

[Project Wiki Page](https://bitbucket.org/theboredbros/source/wiki/Home)

[YouTube Video](https://www.youtube.com/watch?v=KHuwPriVj1I&t=19s)

[Android App Source Code](https://bitbucket.org/theboredbros/source/src/9c20e408e0f928506f2eb67558b9df443096dac2/LED/app/src/main/java/com/boredbros/led/?at=master)

[RPi Source Code](https://bitbucket.org/theboredbros/source/src/9c20e408e0f928506f2eb67558b9df443096dac2/RPi%20Source/Project.py?at=master&fileviewer=file-view-default)

The LED folder is the entire Android Studio Project folder. This includes the app layout, debuggin files, source code, and other miscellaneous files needed to create the android .APK file. The source code that I wrote can be found in the link above.

#Andrew's Labs 6-10

[Lab 6](https://bitbucket.org/theboredbros/source/wiki/Lab%206)
[Lab 7](https://bitbucket.org/theboredbros/source/wiki/Lab%207)
[Lab 8](https://bitbucket.org/theboredbros/source/wiki/Lab%208)
[Lab 9](https://bitbucket.org/theboredbros/source/wiki/Lab%209)
[Lab 10](https://bitbucket.org/theboredbros/source/wiki/Lab%2010)